package uitest.sampler.test;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends MyTestRunner {
    @Story("Be able init session in the taskit site")
    @Severity(SeverityLevel.BLOCKER)
    @Description("Description test: init session in the taskit site with correct system user")
    @Test(description = "Init session in the taskit site with correct system user")
    public void signInWikipedia_In_Right_Case() {
        objLogin.loginToWikipedia(username,password);
        Assert.assertTrue(objLogin.successValidation());
    }

    @Story("Validation user obligatory field")
    @Severity(SeverityLevel.NORMAL)
    @Description("Validation user obligatory field")
    @Test(description = "")
    public void signInWikipedia_Without_Mail(){
        objLogin.loginToWikipedia("","12345678");
        Assert.assertFalse(objLogin.successValidation());

    }
    @Story("Validation obligatory password field")
    @Severity(SeverityLevel.BLOCKER)
    @Description("Be able to see text validation when insert correct username without password")
   // @Test(description = "Be able to see text validation when insert username without password")
    public void loginToWikipedia_Without_Password(){
        objLogin.loginToWikipedia(username,"");

    }
    @Story("Validation obligatory username field")
    @Severity(SeverityLevel.NORMAL)
    @Description("Be able to see text validation when insert correct password without username")
   // @Test(description = "Be able to see text validation when insert correct password without username")
    public void loginToWikipedia_Without_Mail_And_Password(){
        objLogin.loginToWikipedia("","");

    }
    @Story("Validation correct insertion of username field")
    @Severity(SeverityLevel.NORMAL)
    @Description("Validation username field where insert a incorrect text")
   // @Test(priority = 1,description = "Validation username field where insert a incorrect text")
    public void loginToWikipedia_With_Invalid_Mail(){
        objLogin.loginToWikipedia("alex@mail","12345678");

    }
    @Story("Validation the correct insertion of password")
    @Severity(SeverityLevel.NORMAL)
    @Description("Be able to watch validation text when insert a short password")
   // @Test(description = "Be able to watch validation text when insert a short password")
    public void sloginToWikipedia_With_Short_Password(){
        objLogin.loginToWikipedia(username,"1");

    }
    @Story("Not able of init sesion with incorrect not registred")
    @Severity(SeverityLevel.BLOCKER)
    @Description("Sing in to taskit with information not existing to show messagge where validated the information not registred")
   // @Test(description = "Sing in to taskit with information not existing")
    public void loginToWikipedia_With_Information_Not_Existing(){
        objLogin.loginToWikipedia("alejandro@alejandro.com","contraseña");
    }


    @Override
    protected void initObjects(WebDriver driver){  }




}

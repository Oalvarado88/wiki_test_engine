package uitest.sampler.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;
import uitest.sampler.pageObjects.LoginPage;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public abstract class MyTestRunner {
    protected LoginPage objLogin;
    private WebDriver driver;
    String url,username,password,msCore,msRules,osName,seleniumRemoteUrl;
    ChromeOptions options;




    @BeforeMethod
    public void setUp(){
       // try {
        //    findSO();
        //}catch (Exception e){}

        initChromeDriver(1);
        getStrings();
        objLogin = new LoginPage(driver);
        driver.get(url);
        initObjects(driver);
        System.out.println("------------BEFORE------>");
    }

    public void waitSecconds(int secconds){try{Thread.sleep(secconds*1000);}catch (Exception e){}   }

    @AfterMethod
    public void closeConnection(){
        driver.close();
        driver.quit();
        System.out.println("-----------AFTER------>");
    }

    private void findSO() throws MalformedURLException {
        if(seleniumRemoteUrl != null){
            driver = new RemoteWebDriver(new URL(seleniumRemoteUrl), DesiredCapabilities.chrome());
        }else {
            if(osName.contains("Windows")){
                System.setProperty("webdriver.chrome.driver","driver\\chromedriver92.exe");
            }else {
                System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
            }
        }
    }

    private void getStrings(){
        osName = System.getProperty("os.name");
        seleniumRemoteUrl = System.getenv("selenium_remote_url");
         osName = System.getProperty("os.name");
        username="Alalvarado88";
        password = "micontra123";
        url = "https://es.wikipedia.org/wiki/Wikipedia:Portada";
    }

    private void initChromeDriver(int form){

        if(form == 1){
            System.setProperty("webdriver.chrome.driver","driver\\chromedriver92.exe");
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }else{
            options = new ChromeOptions();
            options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors", "--silent");
            driver = new ChromeDriver(options);
        }
    }



    protected abstract void initObjects(WebDriver driver);

}

package uitest.sampler.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import uitest.sampler.utils.WebDriverUtils;

public class LoginPage {

    private  WebDriver driver;
    private WebDriverWait wait;

    @FindBy(xpath = "//*[@id='pt-login']/a")
    WebElement btnAcceder;

     public LoginPage(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver,5);
        PageFactory.initElements(driver,this);
    }
    private void enterLoginPage(){
        wait.until(ExpectedConditions.visibilityOf(btnAcceder));
        btnAcceder.click();
    }

    public void loginToWikipedia(String strUserName, String strUserPassword) {

         try {
             enterLoginPage();
             Thread.sleep(3000);
             WebElement nameField = driver.findElement(By.xpath("//*[@id='wpName1']"));
             wait.until(ExpectedConditions.elementToBeClickable(nameField));
             nameField.sendKeys(strUserName);
             driver.findElement(By.xpath("//*[@id='wpPassword1']")).sendKeys(strUserPassword);
             driver.findElement(By.xpath("//*[@id='wpLoginAttempt']")).click();
         }catch (Exception ex){

         }
    }
    public boolean successValidation(){

            WebElement userLabel = driver.findElement(By.xpath("//*[@id='pt-userpage']/a"));
            wait.until(ExpectedConditions.visibilityOf(userLabel));
             if (userLabel==null){
                 return false;
             }else return true;
    }
}

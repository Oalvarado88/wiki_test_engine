package uitest.sampler.DataAccess;

import java.sql.*;

public class DbClient {
    ResultSet rs;
    String url, user, password, port;
    DbConnection objConnection;
    Connection connection;

    public DbClient() {
        url = "localhost";
        user = "postgres";
        port = "5432";
        password = "generic.password";
    }

    public void resultQuery(String database, String sql) {

        objConnection = new DbConnection(url, port, database, user, password);

        connection = objConnection.connectDatabase();

        try {

            Statement stmt = connection.createStatement();

            rs = stmt.executeQuery(sql);

            connection.close();

        } catch (Exception e) {
            if (e.getMessage() != null){
                if(!e.getMessage().contains("No results were returned by the query.")){
                    System.err.println(e.getMessage());
                }
            }

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }

        }
    }


    public ResultSet returnData (String database, String sql){

        objConnection = new DbConnection(url, port, database, user, password);

        connection = objConnection.connectDatabase();

        try {

            Statement stmt = connection.createStatement();

            rs = stmt.executeQuery(sql);

            connection.close();

        } catch (Exception e) {
            if (e.getMessage() != null) {
                if(!e.getMessage().contains("No results were returned by the query.")){
                    System.err.println(e.getMessage());
                }
            }
        }

            return rs;
        }

}

package uitest.sampler.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WebDriverUtils {
    public WebElement WaitForElementRender(WebDriver driver,String selector, String type,int time){
        WebElement element = null;
        try{
            switch (type){
                case "id":
                   element = driver.findElement(By.id(selector));
                    break;
                case "name":
                    element = driver.findElement(By.name((selector)));
                    break;
                case "className":
                    break;
                case "xpath":
                    element = driver.findElement(By.xpath(selector));
                    break;
            }

            return element;
        }catch (Exception ex)
        {
            return null;
        }
    }
}
